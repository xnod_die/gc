"use strict";

const EventEmitter = require("events");
const puppeteer = require("puppeteer");
const fetch = require("node-fetch");
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
const uploadsFolder = "./uploads";
const downloadFolder = "./uploads/qrcode";
const QrCode = require("qrcode-reader");
const Jimp = require("jimp");

const Util = require("./util/Util");
const { GcontcWebURL, DefaultOptions, Events } = require("./util/Constants");
const PhoneNumber = require("./structures/PhoneNumber");

/**
 * Starting point for interacting with the WhatsApp Web API
 * @extends {EventEmitter}
 * @param {object} options - GClient options
 * @param {AuthStrategy} options.authStrategy - Determines how to save and restore sessions. Will use LegacySessionAuth if options.session is set. Otherwise, NoAuth will be used.
 * @param {string} options.webVersion - The version of WhatsApp Web to use. Use options.webVersionCache to configure how the version is retrieved.
 * @param {object} options.webVersionCache - Determines how to retrieve the WhatsApp Web version. Defaults to a local cache (LocalWebCache) that falls back to latest if the requested version is not found.
 * @param {number} options.authTimeoutMs - Timeout for authentication selector in puppeteer
 * @param {object} options.puppeteer - Puppeteer launch options. View docs here: https://github.com/puppeteer/puppeteer/
 * @param {number} options.qrMaxRetries - How many times should the qrcode be refreshed before giving up
 * @param {number} options.resultMaxRetries - How many times should the result be refreshed before giving up
 * @param {number} options.takeoverOnConflict - If another whatsapp web session is detected (another browser), take over the session in the current browser
 * @param {number} options.takeoverTimeoutMs - How much time to wait before taking over the session
 * @param {string} options.userAgent - User agent to use in puppeteer
 * @param {string} options.ffmpegPath - Ffmpeg path to use when formating videos to webp while sending stickers
 * @param {boolean} options.bypassCSP - Sets bypassing of page's Content-Security-Policy.
 * @param {object} options.proxyAuthentication - Proxy Authentication object.
 *
 * @fires GClient#qr
 * @fires GClient#authenticated
 * @fires GClient#auth_failure
 * @fires GClient#ready
 * @fires GClient#disconnected
 * @fires GClient#change_state
 */
class GClient extends EventEmitter {
  constructor(options = {}) {
    super();

    this.options = Util.mergeDefault(DefaultOptions, options);

    this.authStrategy = this.options.authStrategy;

    this.authStrategy.setup(this);

    this.pupBrowser = null;
    this.pupPage = null;

    Util.setFfmpegPath(this.options.ffmpegPath);
    // Membuat folder session jika belum ada
    try {
      if (!fs.existsSync(uploadsFolder)) {
        fs.mkdirSync(uploadsFolder);
      }
    } catch (error) {
      console.error("Terjadi kesalahan saat membuat folder:", error);
    }
  }

  /**
   * Sets up events and requirements, kicks off authentication request
   */
  async initialize() {
    let [browser, page] = [null, null];

    await this.authStrategy.beforeBrowserInitialized();

    const puppeteerOpts = this.options.puppeteer;
    if (puppeteerOpts && puppeteerOpts.browserWSEndpoint) {
      browser = await puppeteer.connect(puppeteerOpts);
      page = await browser.newPage();
    } else {
      const browserArgs = [...(puppeteerOpts.args || [])];
      if (!browserArgs.find((arg) => arg.includes("--user-agent"))) {
        browserArgs.push(`--user-agent=${this.options.userAgent}`);
      }

      browser = await puppeteer.launch({ ...puppeteerOpts, args: browserArgs });
      page = (await browser.pages())[0];
    }

    if (this.options.proxyAuthentication !== undefined) {
      await page.authenticate(this.options.proxyAuthentication);
    }

    await page.setUserAgent(this.options.userAgent);
    if (this.options.bypassCSP) await page.setBypassCSP(true);

    this.pupBrowser = browser;
    this.pupPage = page;

    await this.authStrategy.afterBrowserInitialized();

    await page.goto(GcontcWebURL, {
      waitUntil: "load",
      timeout: 0,
      referer: "https://getcontact.com/",
    });

    const INTRO_IMG_SELECTOR = ".hn-user";
    const INTRO_QRCODE_SELECTOR = ".qrcode img";

    // Checks which selector appears first
    const needAuthentication = await Promise.race([
      new Promise((resolve) => {
        page
          .waitForSelector(INTRO_IMG_SELECTOR, {
            timeout: this.options.authTimeoutMs,
          })
          .then(() => resolve(false))
          .catch((err) => resolve(err));
      }),
      new Promise((resolve) => {
        page
          .waitForSelector(INTRO_QRCODE_SELECTOR, {
            timeout: this.options.authTimeoutMs,
          })
          .then(() => resolve(true))
          .catch((err) => resolve(err));
      }),
    ]);

    // Checks if an error occurred on the first found selector. The second will be discarded and ignored by .race;
    if (needAuthentication instanceof Error) throw needAuthentication;

    // Scan-qrcode selector was found. Needs authentication
    if (needAuthentication) {
      const { failed, failureEventPayload, restart } =
        await this.authStrategy.onAuthenticationNeeded();
      if (failed) {
        /**
         * Emitted when there has been an error while trying to restore an existing session
         * @event GClient#auth_failure
         * @param {string} message
         */
        this.emit(Events.AUTHENTICATION_FAILURE, failureEventPayload);
        await this.destroy();
        if (restart) {
          // session restore failed so try again but without session to force new authentication
          return this.initialize();
        }
        return;
      }

      const QR_CONTAINER = ".qrcode img";
      const QR_RETRY_BUTTON = ".q-refresh > a";
      let qrRetries = 0;
      let imagePath;
      let QRCode;
      await page.exposeFunction("qrChanged", async (qr) => {
        const newPage = await browser.newPage();
        const response = await newPage.goto(qr);

        if (!fs.existsSync(downloadFolder)) {
          fs.mkdirSync(downloadFolder);
        }

        if (response.ok()) {
          const randomFileName =
            crypto.randomBytes(16).toString("hex") + ".png";
          imagePath = path.join(downloadFolder, randomFileName);

          const imageBuffer = await response.buffer();
          fs.writeFileSync(imagePath, imageBuffer);
          await newPage.close();

          const image = await Jimp.read(imagePath);
          const qrcode = new QrCode();
          qrcode.callback = (err, value) => {
            if (err) {
              console.error(err);
            } else {
              console.log("Hasil scan QR code:", value.result);
              QRCode = value.result;
              fs.unlinkSync(imagePath);
            }
          };
          qrcode.decode(image.bitmap);
        } else {
          await newPage.close();
        }
        /**
         * Emitted when a QR code is received
         * @event GClient#qr
         * @param {string} qr QR Code
         */
        this.emit(Events.QR_RECEIVED, QRCode);
        if (this.options.qrMaxRetries > 0) {
          qrRetries++;
          if (qrRetries > this.options.qrMaxRetries) {
            this.emit(Events.DISCONNECTED, "Max qrcode retries reached");
            await this.destroy();
          }
        }
      });

      await page.evaluate(
        async function (selectors) {
          const qrCodeElement = document.querySelector(selectors.QR_CONTAINER);
          window.qrChanged(qrCodeElement.src);
          return qrCodeElement ? qrCodeElement.src : null;
        },
        {
          QR_CONTAINER,
          QR_RETRY_BUTTON,
        }
      );

      // Wait for code scan
      try {
        await page.waitForSelector(INTRO_IMG_SELECTOR, { timeout: 0 });
      } catch (error) {
        if (
          error.name === "ProtocolError" &&
          error.message &&
          error.message.match(/Target closed/)
        ) {
          // something has called .destroy() while waiting
          return;
        }

        throw error;
      }
    }
    const authEventPayload = await this.authStrategy.getAuthEventPayload();

    /**
     * Emitted when authentication is successful
     * @event GClient#authenticated
     */
    this.emit(Events.AUTHENTICATED, authEventPayload);

    /**
     * Emitted when the client has initialized and is ready to receive messages.
     * @event GClient#ready
     */
    this.emit(Events.READY);
    this.authStrategy.afterAuthReady();
  }

  /**
   * Send a message to a specific chatId
   * @param {string} countryCode
   * @param {string} phoneNumber
   *
   * @returns {Promise<PhoneNumber>} Message that was just sent
   */
  async searchNumber(countryCode, phoneNumber = {}) {
    let responseData;
    await this.pupPage.evaluate(() => localStorage.clear());
    await this.pupPage.evaluate(() => localStorage.clear());
    await this.pupPage.evaluate(() => localStorage.clear());
      await this.pupPage.evaluate(
        async (countryCode, phoneNumber) => {
          const formElement = document.querySelector(".searchForm");
          document.querySelector(".select-country").value = countryCode; 
          document.querySelector("#numberInput").value = phoneNumber;
          document.querySelector(".g-recaptcha").click();
        },
        countryCode,
        phoneNumber
    );
    const randomFunction =
      Math.random().toString(36).substring(2, 15) + Date.now().toString(36);

    await this.pupPage.exposeFunction(randomFunction, async (resData) => {
      responseData = resData;
    });
    console.log(responseData)
    console.log('clicked...')
    let result = {};
    try {
      await this.pupPage.waitForTimeout(2000);
      await this.pupPage.waitForSelector(".box.r-profile-box", { timeout: 0 });
            // Wait for the profile data to load in the page
      await this.pupPage.waitForFunction(() => {
        const profileBox = document.querySelector(".box.r-profile-box");
        return profileBox && profileBox.querySelector("h1").innerText;
      });
      result = await this.pupPage.evaluate(() => {
        const data = {}
        const profileBox = document.querySelector(".box.r-profile-box");
        let result = {}; // Declare 'result' within the function
        if (profileBox) {
          data.name = profileBox.querySelector("h1").innerText;
          result = {
            status: 'success',
            tags: [{ tag: data.name }]
          };
        }
        return result; // Return result, which is an empty object if the condition is not met
      });
      
      console.log(result);
      console.log('profilebox...');
      const hashValue = await  this.pupPage.evaluate(() => {
        const input = document.querySelector('input[name="hash"]');
        if (input) {
          return input.value;
        } else {
          return null;
        }
      });
      const cookies = await this.pupPage.cookies();
      const phpSessionIdCookie = cookies.find(cookie => cookie.name === 'PHPSESSID');
      return {'hashValue':hashValue,'cooke':phpSessionIdCookie,'otherdata':result}
    } catch (error) {
      if (
        error.name === "ProtocolError" &&
        error.message &&
        error.message.match(/Target closed/)
      ) {
        return;
      }

      throw error;
    }

    return new PhoneNumber(this, hashValue);
  }
}

module.exports = GClient;
