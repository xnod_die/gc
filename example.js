const { GClient, GLocalAuth } = require("./index");
const express = require('express');
const qrcode = require('qrcode');
const readline = require("readline");
const app = express();
var mysql = require('mysql');
const axios = require('axios');
const http = require('http');
const server = http.createServer(app);
const socketIO = require('socket.io');
var cors = require('cors');
const port = process.env.PORT || 8800;
const fs = require('fs');
const io = require('socket.io')(server, {
  cors: {
    origin: ['http://103.150.120.29:8083', 'http://103.150.120.29:8082', 'http://103.150.120.29:8084'],
    methods: ["GET", "POST"],
    transports: ['websocket', 'polling'],
    credentials: true
  },
  allowEIO3: true
});
const sessions = [];
var useragent = ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36'];
let results = [];
var con = mysql.createConnection({
  host: "103.150.120.29",
  user: "recon",
  password: "Indon3s1417",
  database: "SC",
  charset: 'utf8mb4'
});
con.connect();

const getSessionsFile = function () {
  var sql = "select nama_devices,status_devices from devices;";
  con.query(sql, function (err, result) {
    if (err) throw err;
    json = JSON.stringify(result);
    results = json;
  });
  return results
}
const createSession = function (deviceid,description) {
  var path = 'WWebJS/';
  if (!fs.existsSync(path + `/${deviceid}`)) {
    fs.mkdirSync(path + `/${deviceid}`);
  }

  const client = new GClient({
    // proxyAuthentication: { username: 'username', password: 'password' },
    puppeteer: {
      headless: false,
      args: ['--no-sandbox', '--disable-dev-shm-usage', '--disable-accelerated-2d-canvas',
        '--disable-accelerated-2d-canvas',
        '--no-first-run',
        '--no-zygote',
        '--single-process', // <- this one doesn't works in Windows
        '--disable-gpu']
    },
    authStrategy: new GLocalAuth({
      clientId: deviceid,
      dataPath: path
    }),
  });

  client.initialize();
  client.on("qr", (qr) => {
    // NOTE: This event will not be fired if a session is specified.
    console.log("QR RECEIVED", qr);
    qrcode.toDataURL(qr, (err, url) => {
      io.emit('qr', { id: deviceid, src: url });
      io.emit('message', { id: deviceid, text: 'QR Code ' });
    });
  });

  client.on("authenticated", async () => {
    console.log("AUTHENTICATED");
    console.log('deviceid');
    io.emit('authenticated', { id: deviceid });
    io.emit('message', { id: deviceid, text: 'GC is authenticated!' });
    var sql = "select nama_devices from devices where nama_devices = ?;";
    var vv = [[deviceid]]
    con.query(sql, [vv], function (err, result) {
      if (err) throw err;
      json = JSON.stringify(result);
      console.log(json.length);
      if (json.length <= 2) {
        var sql = "INSERT INTO devices (nama_devices, `status_devices`) VALUES ?";
        var values = [[deviceid, 'connected']]
        con.query(sql, [values], function (err, result) {
          if (err) throw err;
          console.log("Number of records inserted: " + result.affectedRows);
        });
      } else {
        var sql = "update devices set status_devices ='connected' where nama_devices=?;";
        var values = [[deviceid]]
        con.query(sql, [values], function (err, result) {
          if (err) throw err;
          console.log("done");
        });
      }
    });
  });

  client.on("ready", () => {
    console.log("READY");
  });
  ///web express

    // Tambahkan client ke sessions
    sessions.push({
      id: deviceid,
      client: client
    });
  
    // Menambahkan session ke file
    const savedSessions = getSessionsFile();
    
    return client;
}


const init = function (socket) {
  const savedSessions = getSessionsFile();
  if (savedSessions.length > 0) {
    if (socket) {
      socket.emit('init', savedSessions);
    } else {
      savedSessions.forEach(sess => {
        createSession(sess.id, sess.description);
      });
    }
  }
}
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
// Socket IO
io.on('connection', function (socket) {
  init(socket);
  socket.on('create-session', function (data) {
    console.log('Create session: ' + data.id);
    createSession(data.id);
  });
});
app.use(cors())
app.get('/', (req, res) => {
  res.sendFile('coba.html', {
    root: __dirname
  });
});
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
app.get('/check-number', (req, res) => {
  // console.log(req.body.sender);
  console.log(req.query.number);
  const sender = 'BOTGC';
  const number = req.query.number;
  let otherdata = {}
  const foundSession = sessions.find(sess => sess.id === sender);
  if (foundSession) {
    const client = foundSession.client;
    client.searchNumber("ID", number).then((response) => {
      console.log('response')
      console.log(response.hashValue)
      cookie = response.cooke.value
      console.log(cookie)
      otherdata = response.otherdata
      console.log('otherda', response.otherdata)
      var data = 'hash='+response.hashValue+'&phoneNumber=%2B'+number+'&countryCode=ID';
      var config = {
        method: 'post',
        url: 'https://web.getcontact.com/list-tag',
        headers: { 
          'authority': 'web.getcontact.com', 
          'accept': 'application/json, text/javascript, */*; q=0.01', 
          'accept-language': 'en-US,en;q=0.9', 
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8', 
          'cookie': 'PHPSESSID='+cookie, 
          'origin': 'https://web.getcontact.com', 
          'referer': 'https://web.getcontact.com/search', 
          'sec-fetch-dest': 'empty', 
          'sec-fetch-mode': 'cors', 
          'sec-fetch-site': 'same-origin', 
          'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36', 
          'x-requested-with': 'XMLHttpRequest'
        },
        data : data
      };
      axios(config)
      .then(function (response) {
        if(response.data['status'] == 'error'){
          res.status(200).json({
            status: true,
            info:'notags',
            response: otherdata
          });
        }
        console.log(response.data['status']);
        res.status(200).json({
          status: true,
          info:'valid',
          response: response.data
        });
      })
      .catch(function (error) {
        console.log('error');
      });
    }).catch(err => {
      res.status(500).json({
        status: false,
        response: err
      });
    });

  } else {
    console.error(`Session with id '${sender}' not found.`);
  }
});

var sql = "select id,nama_devices,status_devices from devices;";
con.query(sql, function (err, result) {
  if (err) throw err;
  json = JSON.stringify(result);
  result.forEach(hasil => {
    var idsession = hasil.nama_devices;
    console.log(idsession);
    new createSession(idsession);
  });
});
server.listen(port, function () {
  console.log('App running on *: ' + port);
});
